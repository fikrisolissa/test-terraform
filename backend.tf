provider "aws" {
  profile = "lab"
  region = "ap-southeast-1"
  version = ">= 2.28.1"
}

terraform {
  backend "s3" {
    bucket = "diamunfs-state-bucket"
    key = "terraform"
    region = "ap-southeast-1"
    dynamodb_table = "diamunfs-terraform-lock"
  }
}