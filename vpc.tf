module vpc {
  source  = "terraform-aws-modules/vpc/aws"
  version = "v2.62.0"
  name            = "diamunfs-vpc"
  cidr            = "10.0.0.0/16"
  azs             = ["ap-southeast-1a"]
  public_subnets  = ["10.0.1.0/24"]
  private_subnets = ["10.0.11.0/24"]
  enable_nat_gateway     = true
  single_nat_gateway     = false
  one_nat_gateway_per_az = true
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true
  flow_log_max_aggregation_interval    = 60
}
  



