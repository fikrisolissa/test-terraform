# scale up alarm

resource "aws_autoscaling_policy" "diamunfs-asg-cpu-policy" {
  name                   = "diamunfs-asg-cpu-policy"
#   autoscaling_group_name = aws_autoscaling_group.diamunfs-asg-autoscaling.name
  autoscaling_group_name = module.asg.autoscaling_group_name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "diamunfs-asg-cpu-alarm" {
  alarm_name          = "diamunfs-asg-cpu-alarm"
  alarm_description   = "diamunfs-asg-cpu-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "45"

  dimensions = {
    "AutoScalingGroupName" = module.asg.autoscaling_group_name
  }

  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.diamunfs-asg-cpu-policy.arn]
}

# scale down alarm
resource "aws_autoscaling_policy" "diamunfs-asg-cpu-policy-scaledown" {
  name                   = "diamunfs-asg-cpu-policy-scaledown"
  autoscaling_group_name = module.asg.autoscaling_group_name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "-1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "diamunfs-asg-cpu-alarm-scaledown" {
  alarm_name          = "diamunfs-asg-cpu-alarm-scaledown"
  alarm_description   = "diamunfs-asg-cpu-alarm-scaledown"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "5"

  dimensions = {
    "AutoScalingGroupName" = module.asg.autoscaling_group_name
  }

  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.diamunfs-asg-cpu-policy-scaledown.arn]
}